<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post("add-vendor","API@addVendor");
Route::post("remove-vendor","API@removeVendor");
Route::post("save-complaint","API@saveComplaint");
Route::post("jobs","API@jobs");
Route::post("fliter-jobs","API@filterJobs");
Route::get("all-jobs","API@getAllJobs");
