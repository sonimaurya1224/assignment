<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','salary','join_date','experience','gender','skill_expert','vendor_id','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     public function role(){
        return $this->belongsTo("App\Role","id","role_id");
     }

     public function complaint(){
        return $this->hasMany("App\Complaint","user_id","id");
     }

     public function vendor(){
        return $this->hasMany("App\Vendor","id","id");
     }
}
