<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
     protected $fillable=['role_name','status'];
     
     public function user(){
     	return $this->hasMany("App\User","role_id","id");
     }
}
