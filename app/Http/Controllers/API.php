<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Complaint;
class API extends Controller
{


	public function addVendor(Request $request){
    	try{
              $validate=Validator::make($request->all(), [
            'name' => 'required|string|max:255',
             ]);

           
            if($validate->fails()){
                    return response()->json($validate->errors()->toJson(), 400);
            }
          /*
          if(auth()->user()->role()->role_name==="admin"){
	      Vendor::create(["name"=>$request->name]);
          }
          */

             return response()->json(["status"=>201,"msg"=>"vendor added Successfully!!"]);
    	}
    	catch(\Exception $ex){
           return response()->json(["status"=>500,"msg"=>"Internal Server Error!!"]);
    	}

    }

    public function removeVendor(Request $request){
    	try{
              $validate=Validator::make($request->all(), [
            'vendor_id' => 'required|numeric',
             ]);

           
            if($validate->fails()){
                    return response()->json($validate->errors()->toJson(), 400);
            }


         /*
          if(auth()->user()->role()->role_name==="admin"){
	      Vendor::where("id"=>$request->vendor_id)->delete();
          }
          */
             return response()->json(["status"=>200,"msg"=>"vendor removed Successfully!!"]);
    	}
    	catch(\Exception $ex){
           return response()->json(["status"=>500,"msg"=>"Internal Server Error!!"]);
    	}

    }

    public function saveComplaint(Request $request){
    	try{
              $validate=Validator::make($request->all(), [
            'complaint_type' => 'required|string|max:255',
            'description' => 'required|text',
             ]);

           
            if($validate->fails()){
                    return response()->json($validate->errors()->toJson(), 400);
            }


         /*Complaint::create(["user_id"=>auth()->user()->id,"complaint_type"=>$request->complaint_type,"description"=>$request->description,"vendor_id"=>auth()->user()->vendor_id]);*/
             return response()->json(["status"=>201,"msg"=>"complaint save Successfully!!"]);
    	}
    	catch(\Exception $ex){
           return response()->json(["status"=>500,"msg"=>"Internal Server Error!!"]);
    	}

    }

     public function jobs(Request $request){
    	try{
              $validate=Validator::make($request->all(), [
            'user_id' => 'required|numeric',
             ]);

           
            if($validate->fails()){
                    return response()->json($validate->errors()->toJson(), 400);
            }
           /* if(auth()->user()->id==$request->user_id){*/
        /* $jobs=Job::where("user_id"=$request->user_id)->get();*/
         /*}*/
         $jobs=["job_name"=>"create a api","description"=>"create a payroll system api","time"=>"2pm to 5pm","days"=>"2 days","status"=>"In progress","type"=>"api","created_at"=>\Carbon\Carbon::parse(Carbon\Carbon::now())];


             return response()->json(["status"=>200,"msg"=>"complaint deleted Successfully!!","data"=>$jobs]);
    	}
    	catch(\Exception $ex){
           return response()->json(["status"=>500,"msg"=>"Internal Server Error!!"]);
    	}

    }


    public function filterJobs(Request $request){
    	try{
              $validate=Validator::make($request->all(), [
            'search' => 'required|string|max:255',
             ]);

           
            if($validate->fails()){
                    return response()->json($validate->errors()->toJson(), 400);
            }
           /* 
           $jobs="";

           if(auth()->user()->id==$request->user_id){
           	if (DateTime::createFromFormat('Y-m-d H:i:s', $myString) !== FALSE) {
           $jobs=Job::where("created_at"=DateTime::createFromFormat('Y-m-d H:i:s', $request->search)->get();
             }
             else{
	         $jobs=Job::where("status",'LIKE', '%'.$request->search.'%')->orWhere("type",'LIKE', '%'.$request->search.'%')->get();
             }
         }*/
         $search_jobs=["job_name"=>"create a api","description"=>"create a payroll system api","time"=>"2pm to 5pm","days"=>"2 days","status"=>"In progress","type"=>"api","created_at"=>\Carbon\Carbon::parse(Carbon\Carbon::now())];


             return response()->json(["status"=>200,"data"=>$search_jobs]);
    	}
    	catch(\Exception $ex){
           return response()->json(["status"=>500,"msg"=>"Internal Server Error!!"]);
    	}

    }


    public function getAllJobs(){
    	try{
            /*if(auth()->user()->role()->role_name=="admin"){
	          $jobs=Job::get();
	          return response()->json(["status"=>200,"data"=>$jobs]);
            }*/
            $jobs=["job_name"=>"create a api","description"=>"create a payroll system api","time"=>"2pm to 5pm","days"=>"2 days","status"=>"In progress","type"=>"api","created_at"=>\Carbon\Carbon::parse(Carbon\Carbon::now())];


             return response()->json(["status"=>200,"data"=>$jobs]);
    	}
    	catch(\Exception $ex){
           return response()->json(["status"=>500,"msg"=>"Internal Server Error!!"]);
    	}

    }
}
