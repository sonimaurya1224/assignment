<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable=['name','status'];
     
     public function user(){
     	return $this->belongsTo("App\User","vendor_id","id");
     }

       public static function boot() {
        parent::boot();
        self::deleting(function($vendor) { 
             $vendor->user()->each(function($user) {
                $user->delete(); // <-- direct deletion
             });
         });
    }
}
