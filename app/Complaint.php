<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
      protected $fillable=['user_id','complaint_type','description'];
     
     public function user(){
     	return $this->belongsTo("App\User","id","user_id");
     }
}
